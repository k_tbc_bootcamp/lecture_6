package com.lkakulia.lecture_6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // the main way of implementing the functionality is through XML onClick attributes.
        // to use other ways, delete the onClick attributes in the XML file
        // and uncomment either init function.

        // way 2
        //init()

        // way 3
        //init2()

    }

    fun fromXMLDecrement(v: View?) {
        minus()
    }

    fun fromXMLIncrement(v: View?) {
        plus()
    }

    private fun minus() {
        var number = textView.text.toString().toInt()

        if (number != 0) {
            number--
            textView.text = number.toString()
        }
    }

    private fun plus() {
        var number = textView.text.toString().toInt()

        if (number != 10) {
            number++
            textView.text = number.toString()
        }
    }

    private fun init() {
        minusBtn.setOnClickListener {
            minus()
        }

        plusBtn.setOnClickListener {
            plus()
        }
    }

    private fun init2() {
        minusBtn.setOnClickListener(this)
        plusBtn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.minusBtn -> minus()
            R.id.plusBtn -> plus()
        }
    }

}
